<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
	//zabezpieczenie przed wprowadzniem blędnych danych
	protected $fillable = ['body', 'category_id'];

	public function by(User $user)
	{
		$this->user_id = $user->id;
	}

	//powiązanie z tabelka card
    public function card()
    {
    	return $this->belongsTo(Card::class);
    }
    //powiązanie z tabelka user
     public function user()
    {
    	return $this->belongsTo(User::class);
    }
    //powiązanie z tabelką kategorii
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    //powiązanie z tabelką tagów
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoble');
    }
}
