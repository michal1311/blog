<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    public function notes()
    {
    	return $this->hasMany(Note::class);
    }

    public function addNote(Note $note, $userId)
    {
    	//ustawiamy notatke do ID konkretnego użytkownika
    	$note->user_id = $userId;
    	return $this->notes()->save($note);
    }

      //powiązanie z tabelka user
     public function user()
    {
        return $this->belongsTo(User::class);
    }
}
