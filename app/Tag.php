<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function posts()
    {
    	return $this->belongsToMany('App\Note');
    }

    public function seo()
	{
	    return $this->morphMany('App\Seo', 'seoble');
	}
}
