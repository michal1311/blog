<?php

namespace App\Http\Controllers;
use Illuminate\Contracts\Auth\Guard;


use App\Card;
use App\User;
use App\Note;
use App\Category;
use App\Tag;
use MetaTag;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class CardsController extends Controller
{
   public function index(Request $request, Guard $auth)
   {
    $request->session()->flash('status', 'Task was successful!');
      $user = $auth->user();
   	$card = $user->card;
      if(!$card) {
         $card  = new Card();
         $card->user_id = $user->id;
         $card->title = 'User\'s card ' . $user->email;
         $card->save();
      }

       // Section description
        MetaTag::set('title', 'User card');
        MetaTag::set('description', 'This is user\'s card creator!');

   	return view('cards.index', compact('card'));
   }

   public function show(Guard $auth, $id)
   {
     
      $user = $auth->user();
      $card = Card::find($id);
      
      if(!$card) {
         $card  = new Card();
         $card->user_id = $user->id;
         $card->title = 'User\'s card ';
         $card->save();
      }
      else
      {
         if($card->user_id != $user->id)
         {
            return redirect('/');
         }
      }
      //tworzenie nowej zmiennej która pomaga ustawić stronnicowanie
      $notes = Note::where('card_id', $card->id)->paginate(5);

      $categories = Category::all();
      $tags = Tag::all();

      // Section description
        MetaTag::set('title', 'User card');
        MetaTag::set('description', 'Site presents user\'s card!');

		return view('welcome', compact('card', 'notes', 'categories', 'tags'));
   }
}
