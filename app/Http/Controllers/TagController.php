<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tag;
use Session;
use MetaTag;

class TagController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //pobieramy wszystkie tagi z baz danych i przypisujemy je do zmiennej $tag
        $tags = Tag::all();


        MetaTag::set('title', 'Show all tags');
        MetaTag::set('description', 'You can show existing tags');
        //zwracamy widok tags.index ze wszystkimi pobranmi tagami z bazy
        return view('tags.index')->withTags($tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array('name' => 'required|max:255'));
        $tag = new Tag;
        $tag->name = $request->name;
        $tag->save();
        
        Session::flash('success', 'New Tag was successfully created!');

        return redirect()->route('tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tag = Tag::find($id);

        MetaTag::set('title', 'Show tag\'s details');
        MetaTag::set('description', 'You are watching all tag\'s posts.');

        return view('tags.show')->withTag($tag);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::find($id);

        MetaTag::set('title', 'Edit tag\'s name');
        MetaTag::set('description', 'You are editing tag\'s name.');

        return view('tags.edit')->withTag($tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);

        $this->validate($request, ['name' => 'required|max:255']);

        $tag->name = $request->name;
        $tag->save();

        Session::flash('success', 'Succesfully saved nwe tag!');

        MetaTag::set('title', 'Update tag\'s name');
        MetaTag::set('description', 'You are updating tag\'s name.');

        return redirect()->route('tags.show', $tag->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // pobieramy obiekt po danym id
        $tag = Tag::find($id);
        
        //usuwa wszystkie referencje do danego tagu
        $tag->posts()->detach();

        $tag->delete();

        Session::flash('success', 'Tag was successfully deleted!');

        MetaTag::set('title', 'Delete tag');
        MetaTag::set('description', 'You are deleting tag.');
        
        return redirect()->route('tags.index');
    }
}
