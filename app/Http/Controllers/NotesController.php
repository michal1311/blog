<?php

namespace App\Http\Controllers;

use App\Card;
use App\Note;
use App\Category;
use App\Tag;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\StoreNoteRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Paginator;
use Illuminate\Support\Facades\Session;
use MetaTag;
use App\Seo;

use Illuminate\Http\Request;

// use App\Http\Requests;

class NotesController extends Controller
{
   public function index($id)
   {
      // tworzenie zmiennej i składowanie w niej wszystkich wpisów pobranch z bazy danych
      $note = Note::findOrFail($id);

      // zwracamy widok konkretngo postu
      return view('notes.index')->withNote($note);
   }


   public function store(StoreNoteRequest $request, Card $card, Guard $auth)
   {
      
      $input = $request->all();

      $note = new Note($input);

      // wyświetlenie obrazka jeśli jest w bazie
      if($request->hasFile('uploadedfile')){
         $path = 'storage/' . $request->file('uploadedfile')->store('images', 'public');
         $note->image = $path;
      }
      
 
      $user = $auth->user();
   	$card->addNote($note, $user->id);

      //synchronizacja tagów z dodawaną notatką
      if(isset($request->tags))
      {
         $note->tags()->sync($request->tags);
      }

      //dodawanie meta tagów opisu, słów kluczowych oraz tytułu konkretnego wpisu
      $seo = new Seo();
      $seo->title = $input['title'];
      $seo->description = $input['description'];
      $seo->keywords = $input['keywords'];
      $note->seo()->save($seo);

		return back();
   }

   public function edit(Request $request, Note $note)
   {
         //pobiera wszystie kategrie z bazy
         $categories = Category::all();
         //pobiera wszystkie tagi z bazy
         $tags = Tag::all();
        
         // przechowywanie tagów w tablicy 
         $tags2 = array();
         foreach ($tags as $tag) {
           $tags2[$tag->id] = $tag->name;
         }

   		return view('notes.edit', compact('note', 'categories', 'tags', 'tags2'));
   }

   public function update(Request $request, Note $note)
   {
      $input = $request->all();

      if($request->hasFile('featured_file'))
      {
      //usuwamy stare zdjęcie i dodajemy nowe
       $path = 'storage/' . $request->file('featured_file')->store('images', 'public');
       
      $oldFilename = $note->image;
      //przypisanie do obiektu
      $note->image = $path;
      //usuwanie starego zdjęcia
      Storage::delete($oldFilename);
      }

      //aktualizacja meta tagów dscrption, keywords, title
      $seo = Seo::find($note->seo->first()->id);
      
      $seo->title = $input['title'];
      $seo->description = $input['description'];
      $seo->keywords = $input['keywords'];
      $note->seo()->save($seo);

      //aktualizacja danych
		$note->update($input);

      //usuwanie powiązań tagów z postami
      $note->tags()->detach();
      
      //synchronizacja tagów z dodawaną notatką
      if(isset($request->tags))
      {
         $note->tags()->sync($request->tags);
      }

      $request->session()->flash('success', 'The post was successfully updated.');
      
		return back();
   }

   public function destroy($id)
    {
         $note = Note::find($id);
         //usuwanie meta tagów konkretnego wpisu z baz danych
         $seo = Seo::find($note->seo->first()->id);

         $note->seo()->delete($seo);

         $card_id = $note->card_id;

         $note->tags()->detach();

         $note->delete();

         Session::flash('delete_note', "The post was successfully deleted.");
         
         return redirect()->route('cardShow', $card_id);
    }
}
