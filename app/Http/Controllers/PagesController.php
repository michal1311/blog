<?php

namespace App\Http\Controllers;


class PagesController extends Controller
{
	
	public function home()
	{
	//deklaracja tablicy oraz przypisanie jej wartości
	$people = ['Taylor', 'Matt', 'Jeffrey'];
	//dane z tablicy sa wyświetlane jako drugi argument w funkcji view
	//dzieki takiemu zapisowi dostajemy dostęp do elementów tablicy
	//możemy wykorzstac funkcje compact('people') co daje dostęp do tablicy
    return view('welcome', compact('people'));
	}    

	public function about()
	{
		return view('about');
	}
}
