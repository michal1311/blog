<?php

namespace App\Http\Middleware;

use Closure;

class MustBeAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = $request->user();

        // jeśli jestesm zalogowanie to dostaniemy zmienną $user i nazwe uzytkownika a jesli nie to wartosc bedzie null
        if($user as $user->name == 'JohnAdministratorDoe'){
             return $next($request);
        }

        // jesli wystąpi jakiś błąd wyświtlamy go
        abort(404, 'No way.');
    }
}
