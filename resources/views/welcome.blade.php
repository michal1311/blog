<!-- Dodaje layout do strony -->
@extends('layout')

@section('title')
   {{ isset($seo->title) ? $seo->title : 'Welcome'}}
@stop

@section('header')
	{!! Html::style('css/select2.min.css') !!}
@stop

<!-- Odnosi się do identyfikatora content z layout.blade.php -->
@section('content')

<div class="row">
	<div class="col-md-6 col-md-offset-3">

		<h1>{{ $card->title }}</h1>

		
		@if (Session::get('delete_note'))
			<div class="alert alert-info">
				{{ Session::get('delete_note') }}
			</div>
		@endif


	@if($card)

		<!--wyświetlenie ciała notatek karty jako wypunktowanie -->
		<ul class="list-group">
		@foreach ($notes as $note)
			<li class="list-group-item">

			{{ $note->created_at }}

			<span style="padding-left:68px; font-weight: bold;">Category: {{ $note->category->name }}</span>

			<a href="#" class="pull-right"> {{ $note->user->name }} </a><br />

			<!-- dodanie linku do edycji notatek, w web.php trzeba dodac na końcu ścieżki ->name('editNote') -->
			
			{{ Form::open(['method' => 'DELETE', 'route' => ['notes.destroy', $note->id]]) }}
				{{ Form::submit('Delete', ['class' => 'btn btn-danger pull-right', 'style' => 'margin-bottom:20px; margin-left:5px'])}}
			{{ Form::close() }}

			<a href="{{ URL::Route('editNote', array('note' => $note)) }}" class="btn btn-primary pull-right">Edit note</a>
			

			<!-- dodanie obrazka jeśli jest wstawony z postem -->
			@if($note->image)
			<img src="{{ asset($note->image) }}" style="width: 100%; max-width: 200px; height: auto;"/>
			@endif

			<!-- skrócenie treści wpisu do 96 znaków -->
			<div style="word-wrap: break-word;">
				{!! substr($note->body, 0, 96) !!} {{ strlen($note->body) >96 ? "..." : "" }}
			</div>
			<a href="{{  URL::Route('showNote', array('note' => $note)) }}">[Show post]</a>

			<!-- Wyświetlenie tagów -->
			<div class="tags">
				@foreach($note->tags as $tag)
					<span class="label label-default"> {{ $tag->name }}</span>
				@endforeach
			</div>

			<!-- Wyświetlenie opisu -->
			<div class="metatags_desc">
				
			</div>
			</li>
		@endforeach
		</ul>
		<!-- dodanie stronnicowania, dzięki temu pojawią podstrony, które podzielą wpis-->
		{{ $notes->links() }}
		<hr>

	@endif
		<h3>Add a New Note</h3>

		<form method="POST" action="/cards/{{ $card->id }}/notes" enctype="multipart/form-data">
	<!-- csrf_field() - jest to rodzaj zabezpieczenia formularza który buduje pole input dla nas-->
			{{ csrf_field() }}

			<div class="form-group">
			<!-- stworzenie pola wyboru kategorii -->
			<label>Category:</label>
			<select class="form-control" name="category_id">
				@foreach($categories as $category)
				<option value="{{ $category->id }}">{{ $category->name }}</option>
				@endforeach
			</select>

			<label>Tags:</label>
			<select class="form-control select2-multi" name="tags[]" multiple="multiple">
				@foreach($tags as $tag)
				<option value="{{ $tag->id }}">{{ $tag->name }}</option>
				@endforeach
			</select>

			<!-- Opis wpisu -->
			<label for="description">Description:</label>
			<input type="text" id="description" class="form-control" name="description" value="" />

			<!-- Słowa kluczowe wpisu -->
			<label for="keywords">Keywords:</label>
			<input type="text" id="keywords" class="form-control" name="keywords" value="" />

			<!-- tytuł wpisu -->
			<label for="title">Title:</label>
			<input type="text" id="title" class="form-control" name="title" value="" />
			<!--
			wpisując między tagi <textarea> {{ old('body') }} sprawiam że to co wpisał użytkownik a pozniej kliknął przycisk i dane nie przeszły przez walidację to wszystko pozostanie po odświeżeniu strony jeżeli mielibyśmy inne pole input to możemy stosować tą zasadę przykładowa dla wprowadzenia użytkownika
			<input type="text" name="username" value="{{ old('username') }}">
			-->
			<label>Add post picture</label>
			<!-- ustalenie maksymalnego rozmiaru pliku -->
			<input type="hidden" name="MAX_FILE_SIZE" value="100000" />
			<input name="uploadedfile" type="file" />
				<textarea name="body" class="form-control">{{ old('body') }}</textarea>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Add note</button>
			</div>
		</form>

		<!--jeśli pojawiają się jakies błędy wyświetl je, jest to mechanizm potrzebn do walidacji danych-->
		@if (count($errors) > 0)
		<ul>
			@foreach ($errors->all() as $error) 
				<li>{{ $error }}</li>
			@endforeach
		</ul>
		@endif

	</div>
</div>
@stop

@section('scripts')
 <script type="text/javascript" src="/js/select2.min.js"></script>

 <script type="text/javascript">
 	$('.select2-multi').select2();
 </script>
@stop