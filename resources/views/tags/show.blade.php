<!-- Dodaje layout do strony -->
@extends('layout')

@section('title', "$tag->name")

@section('content')

	<div class="row">	
		<div class="col-md-8">
			<h1>{{ $tag->name }} has <small>{{ $tag->posts()->count() }} notes</small></h1>
		</div>
		<div class="col-md-2">
			{{ Form::open(['route' => ['tags.destroy', $tag->id], 'method' => 'DELETE']) }}
				{{ Form::submit('Delete', ['class' => 'btn btn-danger pull-right', 'style' => 'margin-bottom:20px; margin-left:15px'])}}
			{{ Form::close() }}
		</div>
		<div class="col-md-2">
			<a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-primary pull-right" style="margin-bottom:20px;">Edit</a>
		</div>

	</div>

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Body</th>
						<th>Tags</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($tag->posts as $post)
					<tr>
						<th>{{ $post->id }}</th>
						<td>
						<img src="{{ asset($post->image) }}" style="width: 100%;max-width: 200px; height: auto; float: left; margin-right: 6px;"/>
						
						<div style="word-wrap: break-word;">
							{!! $post->body !!}
						</div>
						</td>
						<td>
						@foreach($post->tags as $tag)
							<span class="label label-default">{{ $tag->name }}</span>
						@endforeach
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

@stop