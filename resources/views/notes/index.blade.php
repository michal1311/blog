@extends('layout')

@section('title')
   {{ null !== ($note->seo->first() && $note->seo->first()->title) ? $note->seo->first()->title : 'Post\'s details'}}
@stop

@section('description')
	{{ null !== ($note->seo->first() && $note->seo->first()->description) ? $note->seo->first()->description : ''}}
@stop

@section('keywords')
	{{ null !== ($note->seo->first() && $note->seo->first()->keywords) ? $note->seo->first()->keywords : '' }}
@stop


@section('content')

	{{ $note->created_at }} 
	<span style="padding-left:68px; font-weight: bold;">Category: {{ $note->category->name }}</span>
	<a href="#" class="pull-right"> {{ $note->user->name }} </a><br />

	<!-- dodanie linku do edycji notatek, w web.php trzeba dodac na końcu ścieżki ->name('editNote') -->
		
	{{ Form::open(['method' => 'DELETE', 'route' => ['notes.destroy', $note->id]]) }}
		{{ Form::submit('Delete', ['class' => 'btn btn-danger pull-right', 'style' => 'margin-bottom:20px; margin-left:15px'])}}
	{{ Form::close() }}

	<a href="{{ URL::Route('editNote', array('note' => $note)) }}" class="pull-right btn btn-primary">Edit note</a>
	
	
	@if($note->image)
	<img src="{{ asset($note->image) }}" style="width: 100%; max-width: 200px; height: auto; float: left; margin-right: 6px; padding-top: 30px;"/>
	@endif

	<!-- treścć wpisu -->
	<div style="padding-top: 35px; word-wrap: break-word;">
	{!! $note->body !!}
	</div>

	<!-- pokazanie tagów -->
	<div class="tags">
		@foreach($note->tags as $tag)
			<span class="label label-default"> {{ $tag->name }}</span>
		@endforeach
	</div>

	<!-- dodanie przycisku anuluj -->
	<a href="{{ URL::Route('cardShow', array('card' => $note->card_id)) }}" class="pull-right btn btn-danger">
		Back
	</a>
@stop 
