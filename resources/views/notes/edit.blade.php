@extends('layout')

@section('title')
    Edit note
@stop

@section('header')
{!! Html::style('css/select2.min.css') !!}
@stop

@section('content')

	<h1>Edit the Note </h1>

		@if(session()->has('success'))
			<div class="alert alert-success">
				{!! session('success') !!}
			</div>
		@endif
	

	<form method="POST" action="/notes/{{ $note->id }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('PATCH') }}

		<div class="form-group">

			@if($note->image)
			<div class="col-md-6">
			<img src="{{ asset($note->image) }}" style="width: 100%; max-width: 200px; height: auto; "/>
			</div
			@endif

			<label>Category:</label>
			<select class="form-control" name="category_id">
				@foreach($categories as $category)
				<option value="{{ $category->id }}" @if( $note->category_id === $category->id ) selected @endif>{{ $category->name }}</option>
				@endforeach
			</select>

			<label>Tags:</label>
			<select class="form-control select2-multi" name="tags[]" multiple="multiple">
				@foreach($tags as $tag)
				<option value="{{ $tag->id }}" @if( $note->tag_id === $tag->id ) selected @endif>{{ $tag->name }}</option>
				@endforeach
			</select>

			<!-- Opis wpisu -->
			<label for="description">Description:</label>
			<input type="text" id="description" class="form-control" name="description" value="{{ $note->seo->first()->description }}">

			<!-- Słowa kluczowe wpisu -->
			<label for="keywords">Keywords:</label>
			<input type="text" id="keywords" class="form-control" name="keywords" value="{{ $note->seo->first()->keywords }}">

			<!-- tytuł wpisu -->
			<label for="title">Title:</label>
			<input type="text" id="title" class="form-control" name="title" value="{{ $note->seo->first()->title }}">

			<!-- zdjecie wpisu -->
			<label>Add post picture</label>
			<!-- ustalenie maksymalnego rozmiaru pliku -->
			<input type="hidden" name="MAX_FILE_SIZE" value="100000" />
			<input name="featured_file" type="file" />

			<textarea name="body" class="form-control" id="myTextarea">
			{{ $note->body }}
			</textarea>
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-primary">Update note</button>

			<!-- dodanie przycisku anuluj -->
			<a href="{{ URL::Route('cardShow', array('card' => $note->card_id)) }}" class="pull-right btn btn-danger">
				Cancel / Back
			</a>

		</div>
	</form>
@stop

@section('scripts')
 <script type="text/javascript" src="/js/select2.min.js"></script>

 <script type="text/javascript">
 	$('.select2-multi').select2();
 	$('.select2-multi').select2().val({!! json_encode($note->tags()->getRelatedIds()) !!}).trigger('change');
 </script>
@stop
