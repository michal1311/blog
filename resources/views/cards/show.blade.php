@extends('layout')

@section('title')
    Show card
@stop

@section('content')


<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<h1>{{ $card->title }}</h1>


		<!--wyświetlenie ciała notatek karty jako wypunktowanie -->
		<ul class="list-group">
		@foreach ($card->notes as $note)
			<li class="list-group-item">
			{{ $note->created_at }}
			<a href="#" class="pull-right"> {{ $note->user->name }} </a>
			{!! $note->body !!}
			</li>
		@endforeach
		</ul>

		<hr>
		<h3>Add a New Note</h3>

		<form method="POST" action="/cards/{{ $card->id }}/notes">
		<!-- csrf_field() - jest to rodzaj zabezpieczenia formularza który buduje pole input dla nas-->
			{{ csrf_field() }}

			<div class="form-group">
			<!--
			wpisując między tagi <textarea> {{ old('body') }} sprawiam że to co wpisał użytkownik a poznij kliknął przycisk i dane nie przeszły przez walidację to wszystko pozostanie po odświeżeniu strony jeżeli mielibyśmy inne pole input to możemy stosować tą zasadę przykładoda dla wprowadzenia użytkownika
			<input type="text" name="username" value="{{ old('username') }}">-->
				<textarea name="body" class="form-control">{{ old('body') }}</textarea>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Add note</button>
			</div>
		</form>

		<!--jeśli pojawiają się jakies błędy wyświetl je-->
		@if (count($errors))
		<ul>
			@foreach ($errors->all() as $error) 
				<li>{{ $error }}</li>
			@endforeach
		</ul>
		@endif

	</div>
</div>
@stop