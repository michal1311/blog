@extends('layout')

@section('title')
   {{ isset($seo->title) ? $seo->title : 'User cards'}}
@stop

@section('header')
	<meta name="keywords" content="{{ isset($seo->keywords) ? $seo->keywords : ''}}">
	<meta name="description" content="{{ isset($seo->description) ? $seo->description : ''}}">
@stop

@section('content')

	<h1>All Cards</h1>

<!-- wyświetlenie każdej karty w elemencie div -->
	<a href="/cards/{{ $card->id }}">{{ $card->title }}</a>
	
@stop