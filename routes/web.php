<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'CardsController@index')->middleware('auth');

Route::get('about', 'PagesController@about');

Route::get('cards', 'CardsController@index');

Route::get('cards/{card}', 'CardsController@show')->name('cardShow')->middleware('auth');

Route::post('cards/{card}/notes', 'NotesController@store');
Route::get('notes/{note}/edit', 'NotesController@edit')->name('editNote')->middleware('auth');

Route::delete('notes/{note}', array('as' =>'notes.destroy', 'uses' => 'NotesController@destroy'));

Route::patch('notes/{note}', 'NotesController@update');

Route::get('notes/{note}/show', 'NotesController@index')->name('showNote')->middleware('auth');

Route::resource('categories', 'CategoryController', ['except' => ['create']]);

Route::resource('tags', 'TagController', ['except' => ['create']]);
/*
Route::get('about', function(){
	return view('pages.about'); // ustawienie ścieżki do resources/pages/views/about.blade.php
});
*/
Auth::routes();

Route::get('/dashboard', 'HomeController@index');



Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
